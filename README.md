# Purpose

Setup raspberry pi zero board to install needed software and configuration.


# Board setup


## Host

* Download newest raspian operating system
    https://www.raspberrypi.org/downloads/raspbian/
* Unzip
* Write image to sd card to right disk sd`X`
  ```
  pv ubuntu-19.04-live-server-amd64.iso | sudo dd of=/dev/sd`X` oflag=sync bs=4M
  ```
* Mount boot partition
  ```
  sudo mount /dev/sd`X`1 /mnt/
  ```
* Edit config file to enable uart
  ```
    echo enable_uart=1 | sudo tee --append /mnt/config.txt
  ```
* Enable ssh daemon on boot
  ```
  sudo touch /mnt/ssh
  ```
* Unmount boot partition
  ```
  sudo umount /mnt/
  ```
* Mount system partition
  ```
  sudo mount /dev/sd`X`2 /mnt/
  ```
* Copy ssh key
  ```
  cat ~/.ssh/id_rsa | sudo tee --append /mnt/home/pi/.ssh/authorized_keys
  ```
* Copy this repository
  ```
  sudo cp -r . /mnt/home/pi/
  ```


## Raspberry

* Insert sdcard into raspberry and power on
* Change password
  ```
  passwd
  ```
* Configure wifi network
  ```
  sudo raspi-config
  ```
* Enter this repository
  ```
  cd ~/setup-*
  ```
* Fix permissions
  ```
  sudo chown --recursive pi:pi .
  ```
* Initialize this board

  **WARNING:** ssh password authentication will be disabled
  ```
  sudo ./setup-raspbian-raspberry-pi-zero/install-system
  ```
  ```
  ./setup-raspbian-raspberry-pi-zero/install-user
  ```
